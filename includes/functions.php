
<?php
include "config.php";
function dbSelect($table, $filters=null, $likeFilters=null, $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    global $mysqlConnect;
    $sql = "SELECT * FROM $table";

    if (($filters != null)||($likeFilters != null)){
        $sets = [];
        if ($filters != null) {
            foreach ($filters as $column => $value){
                if ($value != null) {
                    $sets[] = mysqli_real_escape_string($mysqlConnect, $column)."='".mysqli_real_escape_string($mysqlConnect, $value)."'";
                }
            }
        }
        if ($likeFilters != null) {
            foreach ($likeFilters as $column => $value){
                if ($value != null) {
                    $sets[] = mysqli_real_escape_string($mysqlConnect, $column)." LIKE '%".mysqli_real_escape_string($mysqlConnect, $value)."%'";
                }
            }
        }
        $sql.= ' WHERE '.implode(' AND ', $sets);
    }

    if ($sortBy != null) {
        $sql.= ' ORDER BY '.mysqli_real_escape_string($mysqlConnect, $sortBy).' '.mysqli_real_escape_string($mysqlConnect, $sortDirection);
    }

    if ($limit != null){
        $sql.= ' LIMIT '.intval($offset).','.intval($limit);
    }

    $result = mysqli_query($mysqlConnect, $sql);

    if (!$result){
        die("SQL error: " . mysqli_error($mysqlConnect)." SQL:".$sql);
    }

    return $result->fetch_all(MYSQLI_ASSOC);
}


function dbSelectOne($table, $filters=null, $likeFilters=null, $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    $data = dbSelect($table, $filters, $likeFilters, 0, 1,  $sortBy, $sortDirection);
    return $data[0];
}
function dbInsert($table, $data)
{
    global $mysqlConnect;
    $columns = [];
    $values = [];
    $images = $_FILES['picture'];

    foreach ($data as $column => $value) {
        $columns[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "`";
        $values[] = "'" . mysqli_real_escape_string($mysqlConnect, $value) . "'";
    }
    $sqlcolumns = implode(',', $columns);
    $sqlvalues = implode(',', $values);
    $result = mysqli_query($mysqlConnect, "INSERT INTO $table($sqlcolumns) VALUES ($sqlvalues)");
    return mysqli_insert_id($mysqlConnect);
}
function dbDelete($table, $id){
    global $mysqlConnect;
    $sql = "DELETE FROM $table WHERE id=".intval($id);
    $mysqlConnect->query($sql);
    return mysqli_affected_rows($mysqlConnect)>0;

}
function dbUpdate($table, $id, $data){
    global $mysqlConnect;
    $sets = [];
    foreach ($data as $column => $value){
        $sets[]="`".mysqli_real_escape_string($mysqlConnect, $column)."`='".mysqli_real_escape_string($mysqlConnect, $value)."'";
    }
    $sqlSets = implode(',', $sets);
    $sql = "UPDATE $table SET $sqlSets id=".intval($id);
    $mysqlConnect->query($sql);
    return mysqli_affected_rows($mysqlConnect)>0;
}




?>