<?php


class City extends BaseEntity
{


    public $name;

    public $state_id;


    public function getRelations()
    {
        return [
            'productImages' =>[
                'target'=>ProductImage::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'product_id'
            ],
            'category'=>[
                'target'=>Category::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'category_id'
            ]
        ];
    }


}