<?php
include "includes/functions.php";


$filter = [ 'email' => $_POST['email'],
    'password' => $_POST['password']
];

$user = dbSelectOne('user', $filter);

if (is_null($user)){
    header ('Location:  Login.php');
}
else{
    session_start();
    $_SESSION['logedin']=true;

    header ('Location: index.php');
}
?>
