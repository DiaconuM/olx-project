<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 8/21/2019
 * Time: 10:55 PM
 */
class Category extends BaseEntity
{


    public $name;


    public function getRelations()
    {
        return [
            'product' =>[
                'target'=>Product::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'product_id'
            ],
            'category'=>[
                'target'=>Category::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'category_id'
            ]
        ];
    }


    public function getProducts(){

        $data=dbSelect('products', ['category_id'=>$this->id]);

        $result=[];

        foreach($data as $productData){
            $result[]=new Product ($productData['id']);

        }

    }
}