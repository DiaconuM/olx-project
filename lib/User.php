<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 8/21/2019
 * Time: 11:02 PM
 */
class User extends BaseEntity
{


    public $username;

    public $password;


    public function getRelations()
    {
        return [
            'productImages' =>[
                'target'=>ProductImage::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'product_id'
            ],
            'category'=>[
                'target'=>Category::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'category_id'
            ]
        ];
    }

}