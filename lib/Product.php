<?php

class Product extends BaseEntity
{

    const STATUS_ACTIVE='active';

    const STATUS_INACTIVE='inactive';

    const STATUS_UNREAD='unread';




    public $name;
    public $description;
    public $price = 0;
    public $category_id;
    public $city;

    public $status = self::STATUS_ACTIVE;

    public static function getHomepageProducts()
    {
        $data = dbSelect('product',['home_page'=>1]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new Product($productData['id']);
        }

        return $result;
    }
    public function getRelations()
{
    return [
        'category' =>[
            'target'=>Category::class,
            'type' => self::ONE_TO_MANY,
            'link' => 'category_id'
        ],
        'Product'=>[
            'target'=>Product::class,
            'type' => self::MANY_TO_ONE,
            'link' => 'product_id'
        ]
    ];
}

}