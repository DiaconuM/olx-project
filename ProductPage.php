<?php
include "includes/functions.php";
include "lib/State.php";
include "lib/City.php";

$product = new product($_GET['id']);
$user= new user($_GET['id']);
$state= new state($_GET['id']);
$city= new city($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $product->name; ?></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="main.css">
</head>
<body>
<div id="logo" >
            <div id="image-logo" align="left" class="col-12"><a href="index.php"><img src="picture/OLX_Logo.jpg" height="60px" width="100px"
                                                          align="center"></a>
        <a href="AddAd.php" ><div id="logo-button" class="col-3 btn btn-primary center-block float-right"><input
                class="btn btn-primary" type="button"
                value="+ Adauga anunt nou"></div></a>
    </div>


</div>
<hr/>
<div class="container" >

    <div class="row">
        <div id="header" class="col-12">

            <div id="menu-item">


                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Inapoi</a></li>
                        <li class="breadcrumb-item active">
                            Anunturi Ploiesti
                        </li>
                        <li class="breadcrumb-item active" >Electronice si electrocasnice Ploiesti
                        </li>
                        <li class="breadcrumb-item active" >Telefoane Ploiesti</li>

                        <li class="breadcrumb-item active" >iPhone Ploiesti</li>
                    </ol>
                </nav>
            </div>
            <hr/>
        </div>
    </div>
    <div class="row">
        <div id="content" class="col-9">

            <div id="article-image">



                <img src="picture/iphoneimg1.jpg" class="rounded mx-auto d-block " alt="...">
            </div>
            <div id="article-content"><h1><?php echo $product->name; ?></h1></div>
            <a href="#">Ploiesti, judet Prahova </a> <a href="#">Adaugat de pe telefon La 20:39</a>, 16 iulie 2019,
            Numar anunt: 186361492
            <br/>
            <div >
                <button type="button" class="btn btn-primary btn-sm float=left">Small button</button>
                <button type="button" class="btn btn-secondary btn-sm">Small button</button>
            </div>
            <br/>
            <div>
                <button type="button" class="btn btn-primary btn-lg ">Small button</button>
                <button type="button" class="btn btn-secondary btn-lg">Small button</button>
            </div>
            <hr/>
            <div class="row">
                <div class="col-3">Oferit de</div>
                <div class="col-3"><a href="#"> Firma</a></div>
                <div class="col-3">Stare</div>
                <a href="#"> Nou</a>
            </div>
            <hr/>
            <div class="row">
                <div id="description" class="col-12"><p class="text-left">Impecabil / cutie / încărcător original
                        <br/><br/><br/>

                        Garantie Internaționala 9 Noiembrie !<br/><br/><br/>

                        2750 Ron<br/><br/><br/>

                        Nu fac schimburi Nu negociez prețul <br/><br/><br/></p></div>
            </div>

            <div class="row">
                <div id="scroll-content" class="col-12">


                    <p>Anunt adaugat prin aplicatia gratuita OLX.ro, <br/>
                        disponibila pentru Android, iOS</p></div>
            </div>
            <div class="row">
                <div id="add-image" class="col-12">
                    <img src="picture/iphoneimg2.jpg" class="rounded mx-auto d-block" alt="...">
                </div>

                <div class="row col-12">
                    <div id="nav_back" class="col-9">
                        <a href="#"> < Inapoi </a>

                    </div>

                    <div id="nav_forward" >
                        <a href="#"> Urmatorul anunt > </a>

                    </div>
                </div>


            </div>
            <hr/>
            <div  id="content-2" class="row">
                <div class="col-12"><h6>Contacteaza vanzatorul</h6></div>
                <div class="col-12"><button class="btn btn-primary" type="submit"> Nr tel 07xxxxxxxx </button></div>
                <div class="col-12">
                    <form>
                        <div class="col-12">
                            <p>Email adress</p>
                            <div class="col-12">
                                <input type="text" class="form-control" placeholder="email adress">
                            </div>

                        </div>
                    </form>
                    <form>
                        <div class="form-group col-12">
                            <label for="exampleFormControlTextarea1">Mesajul tau</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <button type="button" class="btn btn-primary">Trimite mesaj</button>
                    </form>
                </div>
            </div>
        </div>
        <div id="sidebar" class="col-3">
            <div id="sidebar-1" class="col-12">

                <div id="pret" class="col-12 text-center"><h3>2750 lei</h3></div>

                <br/>

                <div id="pret-2" class="col-12 text-center"> 2750 lei</div>

                <br/>
                <div><a class="btn btn-primary col-12" href="#" role="button">Trimite msg</a></div>
                <br/>
                <div ><a class="btn btn-primary col-12" href="#" role="button">07xxxxxxxxx</a></div>
                <br/>
                <div id="contact" class="col-12 text-center">

                    <div id="localitate" class="col-12"><?php echo $city->name;?> , <?php echo $state->name;?>

                        <div class="col-12" style="margin-bottom: 30px"><a href="#">Vezi pe harta</a></div>
                        <div class="col-12"><img src="picture/avatarimg.jpg"></div>

                    </div>
                    <br/>
                    <div class="col-12" style="margin-bottom: 20px"><a href="#"><?php echo $user->username; ?></a></div>
                    <button type="button" class=" col-12 btn btn-primary  " style=" margin-bottom: 20px">Anunturile utilizatorului</button>

                    <button type="button" class="btn btn-success btn-lg btn-block">Livrare cu verificare</button>
            </div>
            <div id="sidebar-2"> celelalte anunturi ale utilizatorului</div>
        </div>

    </div>


</div>
<div class="col-12 row "> dsadsa</div>
</div>

<?php include "parts/footer.php" ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>