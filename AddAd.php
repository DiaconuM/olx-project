<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/ProductPage.css">
</head>
<body>
<div class="row" id="logo" >
    <div id="image-logo" align="left" class="col-12">
        <a href="bootstrap/index.php"><img src="picture/OLX_Logo.jpg" height="60px" width="100px"></a>
        <a href="AddAd.php"><div id="logo-button" class="col-3 btn btn-primary center-block float-right">
                <input class="btn btn-primary" type="button" value="+ Adauga anunt nou"> </div></a>
    </div>


</div> <hr/>
<form action="dbAdd.php" method="POST">
    <div class="container">

        <div class="row">
            <div id="Adaugare" class="col-12"><h3>Adaugare Anunt</h3><hr/></div>

            <div class="row col-12">
                <div class="col-3"><h5>Titlu*</h5></div>
                <div class="col-9">
                    <form class="form=control">
                        <div class="form-group">
                            <input type="Titlul" class="form-control" name="Titlul" aria-describedby="emailHelp" placeholder="Titlul">
                            <small id="emailHelp" class="form-text text-muted">70 Numar de caractere ramase

                            </small>
                        </div>
                    </form>
                </div>
            </div>


            <div class="row col-12">
                <div class="col-3"><h5>Categoria*</h5></div>

                <div class="col-3">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Categorii
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <button class="dropdown-item" type="button">Telefoane</button>
                            <button class="dropdown-item" type="button">Masini</button>
                            <button class="dropdown-item" type="button">Imobiliare</button>
                        </div>
                    </div>
                </div>
                <div class="col-3"><img src="picture/alegcateg.jpg"></div>
            </div>

            <hr/>

            <div class="row col-12">
                <div class="col-3"><h5>Descriere</h5></div>
                <div class="col-9">

                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"></label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="8">

                        </textarea>
                        <small id="emailHelp" class="form-text text-muted">9000 Numar de caractere ramase

                        </small>
                    </div>


                </div>
            </div>
            <div class="row col-12">
                <div class="col-3"><h5>Fotografii</h5></div>
                <div class="col-9">
                    <div class="row col-12">
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>

                    </div>

                </div>

                <div class="col-3"></div>
                <div class="col-9">
                    <div class="row col-12">
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>
                        <div class="col-3"><a href="#"><img src="picture/fotoclik.jpg" ></a></div>

                    </div>
                </div>

            </div>
            <div class="row col-12">
                <div class="col-12"><h2>Date de contact</h2><hr/> </div>

            </div>
            <div class="row col-12">
                <div class="col-3"><h5>Orasul sau localitatea</h5></div>
                <div class="col-9">
                    <form>
                        <div class="form-group">

                            <input type="text" class="form-control" name="City" aria-describedby="emailHelp" placeholder="City">
                            <small id="emailHelp" class="form-text text-muted">

                            </small>
                        </div>
                    </form>
                </div>
                <div class="col-3"><h5>Judet</h5></div>
                <div class="col-9">
                    <form>
                        <div class="form-group">

                            <input type="text" class="form-control" name="State" aria-describedby="emailHelp" placeholder="State">
                            <small id="emailHelp" class="form-text text-muted">

                            </small>
                        </div>
                    </form>
                </div>
            </div>
            <hr/>
            <div class="row col-12">
                <div class="col-3"><h5>Persoana de contact*</h5></div>
                <div class="col-9">
                    <form>
                        <div class="form-group">

                            <input type="text" class="form-control" name="Contact" aria-describedby="emailHelp" placeholder="Contact">
                            <small id="emailHelp" class="form-text text-muted">70 Numar de caractere ramase

                            </small>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row col-12">
                <div class="col-3"><h5>Adresa de email*</h5></div>
                <div class="col-9">
                    <form>
                        <div class="form-group">

                            <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="E-mail">
                            <small id="emailHelp" class="form-text text-muted">70 Numar de caractere ramase

                            </small>
                        </div>
                    </form>
                </div>
            </div>


            <div class="row col-12">
                <div class="col-3"><h5>Numar de telefon                </h5></div>
                <div class="col-9">
                    <form>
                        <div class="form-group">

                            <input type="text" class="form-control" name="number" aria-describedby="emailHelp" placeholder="number">
                            <small id="emailHelp" class="form-text text-muted">

                            </small>
                        </div>
                    </form>
                </div>
            </div>
            <hr/>
            <div class="row col-12 ">
                <div class="col-12 text-right ">
                    <button type="button" class="btn btn-outline-primary btn-lg">Previzualizare</button>
                    <button type="submit" class="btn btn-primary btn-lg" name="submit" value="Insert">Continua</button>

                </div>

            </div>






        </div>

    </div>


</form>
<?php include "parts/footer.php" ?>
</body>
</html>