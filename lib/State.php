<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 8/21/2019
 * Time: 11:00 PM
 */


class State extends BaseEntity
{


    public $name;


    public function getRelations()
    {
        return [
            'productImages' =>[
                'target'=>ProductImage::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'product_id'
            ],
            'category'=>[
                'target'=>Category::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'category_id'
            ]
        ];
    }

}
