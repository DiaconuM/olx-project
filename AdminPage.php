<html>
<head><?php include"parts/head.php";?><head>
<body>

///header
<div class="container">
<?php include "parts/header.php";?>
<?php include "includes/functions.php";?>

    <div class="container-fluid">

        <div class="row">
            <div id="Adaugare" class="col-12"><h3>Pagina Administrare</h3><hr/></div>

            <div class="row col-12">
                <div class="col-3"><h5>Meniu Admin</h5>

                    <div class="row"><button type="button" class="btn btn-primary btn-block">Category</button></div>
                    <div class="row"><button type="button" class="btn btn-primary btn-block">City</button></div>
                    <div class="row"><button type="button" class="btn btn-primary btn-block">Image</button></div>
                    <div class="row"><button type="button" class="btn btn-primary btn-block">Product</button></div>
                    <div class="row"><button type="button" class="btn btn-primary btn-block">State</button></div>
                    <div class="row"><button type="button" class="btn btn-primary btn-block">User</button></div>



                </div>
                <div class="col-9">
                    Tabel Produse
                    <?php $products = Product::findAll();?>
                    <?php $columns = get_object_vars($products[0]); ?>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <?php foreach ($columns as $column => $value): ?>
                                <th scope="col"><?php echo $column; ?></th>
                            <?php endforeach; ?>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($products as $product): ?>
                            <tr>
                                <?php foreach (get_object_vars($product) as $column => $value): ?>
                                    <td><?php echo $value; ?></td>
                                <?php endforeach; ?>
                                <td>
                                    <a href="edit.php?id=<?php echo $product->getId(); ?>">Edit</a>
                                    <a href="delete.php?id=<?php echo $product->getId(); ?>">Delete</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                </div>







<?php include "parts/footer.php";?>




</body>




</html>