<!DOCTYPE html>
<html >
<!-- head-->
<?php include "parts/head.php"?>
<body >

<!--Header-->
<?php include "parts/header.php"?>
<!--Continut-->
<div class="container">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#home">Intra in cont</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#Menu">Creaza un cont</a>
        </li>
    </ul>
    <!-- Continutul primului Menu-->
    <div class="tab-content">
        <div id="#home" class="container tab-pane active">
            <br>
            <button type="button" class="btn btn-primary">
                <i class="fa fa-facebook">Continua cu contul Facebook</i>
            </button>


            <div class="row">
                <div class="col-12">
                    <p style="text-align:center">sau</p>
                </div>
            </div>
            <form action="checkLogin.php" method="post">
                <div id="home" class="container tab-pane active"><br>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
                    </div>
                </div>

                <div class="row"  style="text-align:left">
                    <div class="col-12"> <a href="#">Ai uitat parola?</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <nav class="navbar navbar-expand-sl bg-primary justify-content-center">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <button type="submit" class="btn btn-default">Login</button>
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-12">
                    <a href="#" style="text-align: center">Alte optiuni de a intra in cont</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>
                        Prin accesarea contului, esti de acord cu <a href="#"> Termenii si Conditiile site-ului</a>
                    </p>
                </div>
            </div>

        </div>

        <!--Continut Meniu 2-->
        <div id="Menu" class="container tab-pane fade"><br>

            <button type="button" class="btn btn-primary">
                <i class="fa fa-facebook">Continua cu contul Facebook</i>
            </button>

            <div class="row">
                <div class="col-12">
                    <p style="text-align:center">sau</p>
                </div>
            </div>

            <div id="home" class="container tab-pane"><br>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p> Prin clic pe butonul Inregistreaza-te, accept <a href="#"> Termenii de utilizare. </a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>Am înțeles că S.C. OLX Online Services S.R.L. îmi folosește datele personale în conformitate cu
                        <a href="#"> Declarația de confidențialitate și Politica privind modulele cookie și alte tehnologii similare </a>. S.C.
                        OLX Online Services S.R.L. recurge la sisteme automate și la parteneri care analizează modul în care
                        utilizez serviciile, pentru a oferi o funcționalitate a produsului și un conținut relevant, materiale publicitare personalizate,
                        precum și protecție împotriva mesajelor nesolicitate, software-ului rău intenționat și utilizării neautorizate. </p>
                </div>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>
            <div class="col-10">
                <p>Sunt de acord cu folosirea mijloacelor de comunicare electronica si a
                    echipamentelor de telecomunicatii in scopul trimiterii de informatii
                    de natura comerciala (de ex. buletine informative, mesaje SMS) privind
                    OLX Online Services S.R.L., afliliatii sau partenerii sai.</p>
            </div>

            <button type="button" class="btn-btn-primary">Inregistreaza-te</button>





        </div>

        <?php include "parts/footer.php" ?>

    </div>
</div>

</body>
</html>

