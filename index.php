<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<!-- head-->
<?php include "parts/head.php"?>
<body >
<div class="container-fluid">
    <!--Header-->
    <?php include "parts/header.php"?>

    <!--Continut-->
    <div class="container">
        <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
            <form class="form-inline" action="">
                <input class="form-control mr-sm-5" type="text" placeholder="4 123 425 anunturi in apropierea ta ">
                <input class="form-control mr-sm-2" type="text" placeholder="Craiova, judetul Dolj">
                <input class="form-control mr-sm-auto " STYLE="background-color:#000066" type="text" placeholder="CAUTA ACUM">

            </form>
        </nav>
        <div >
            <div class="row">
                <div class="col-3">
                    <img src="picture/car.JPG"/>
                    <a href="#"><b>Auto, moto si ambarcatiuni</b></a>
                </div>
                <div class="col-3">
                    <img src="picture/home.JPG"/>
                    <a href="#"> <b>Imobiliare</b></a>
                </div>
                <div class="col-3">
                    <img src="picture/bag.JPG"/>
                    <a href="#"><b>Locuri de munca</b></a>
                </div>
                <div class="col-3">
                    <img src="picture/phone.JPG"/>
                    <a href="#"><b>Electrice si electorcasnice</b></a>
                </div>

            </div>
            <div class="row">
                <div class="col-3">
                    <img src="picture/coat.JPG"/>
                    <a href="#"><b>moda si frumusete </b></a>
                </div>
                <div class="col-3"><img src="picture/sofa.JPG"/>
                    <a href="#"> <b>casa si gradina</b></a>
                </div>
                <div class="col-3">
                    <img src="picture/mum.JPG"/>
                    <a href="#"><b>Mama si copilul</b></a>
                </div>
                <div class="col-3">
                    <img src="picture/bicicle.JPG"/>
                    <a href="#"><b>Sport,timp liber,arta</b></a>
                </div>

            </div>
            <div class="row">
                <div class="col-3">
                    <img src="picture/labuta.JPG"/>
                    <a href="#"><b>Animale de companie </b></a>
                </div>
                <div class="col-3">
                    <img src="picture/agro.JPG"/
                    <a href="#"> <b>Agro si industrie</b></a>
                </div>
                <div class="col-3">
                    <img src="picture/serv.JPG"/>
                    <a href="#"><b>Servicii afaceri echipamente firme</b></a>
                </div>
                <div class="col-3">
                    <a href="#"><b>Oferte de cazare</b></a>
                </div>

            </div>

        </div>

        <div class="row"><h2 stile="backgroung-color: grey">Anunturi promovate</h2></div>
        <div class="row">
            <div class="col-2">
                <div class="row">
                    <img src="picture/iphoneimg1.jpg" width="50%" height="60%" align="center"/>
                </div>
                <div class="row">
                    <a href="ProductPage.php">IPhone X SpaceGray 64GB </a>
                </div>
                <div class="row">
                    <div class="col-6">2275 lei</div>
                    <div class="fas fa-star "></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/teren.JPG" />
                </div>
                <div class="row">
                    <a href="#">Vand inchiriez teren</a>
                </div>
                <div class="row">
                    <div class="col-6"> 70 $</div>
                    <div class="col-6 fas fa-star"> </div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/navigatie.JPG" />
                </div>
                <div class="row">
                    <a href="#">Navigatie BMW</a>
                </div>
                <div class="row">
                    <div class="col-6">3 399 lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/trasport.JPG" />
                </div>
                <div class="row">
                    <a href="#">Servicii Trasport marfa</a>
                </div>
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6 fas fa-star">
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/pisica.JPG" />
                </div>
                <div class="row">
                    <a href="#">DJ Evenimente</a>
                </div>
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/chirie.JPG" />
                </div>
                <div class="row">
                    <a href="#">Inchiriez apartament</a>
                </div>
                <div class="row">
                    <div class="col-6">800lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-2">
                <div class="row">
                    <img src="picture/terenintravilan.JPG"/>
                </div>
                <div class="row">
                    <a href="#">Teren intravilan Malu mare</a>
                </div>
                <div class="row">
                    <div class="col-6">30 000 $</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/mobila.JPG"/>
                </div>
                <div class="row">
                    <a href="#">Mobila comanda Craiova</a>
                </div>
                <div class="row">
                    <div class="col-6">3000 lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/tester.JPG"/>
                </div>
                <div class="row">
                    <a href="#">Tester masini import</a>
                </div>
                <div class="row">
                    <div class="col-6">8000 lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/chei.JPG"/>
                </div>
                <div class="row">
                    <a href="#">Copie cheie masina</a>
                </div>
                <div class="row">
                    <div class="col-6">300 lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/pisica.JPG" />
                </div>
                <div class="row">
                    <a href="#">DJ Evenimente</a>
                </div>
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/chirie.JPG" />
                </div>
                <div class="row">
                    <a href="#">Inchiriez apartament</a>
                </div>
                <div class="row">
                    <div class="col-6">800lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-2">
                <div class="row">
                    <img src="picture/incarcator.JPG" />
                </div>
                <div class="row">
                    <a href="#">Incarcator Samsung fast</a>
                </div>
                <div class="row">
                    <div class="col-6"> 49 lei</div>
                    <div class="fas fa-star "></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/teren.JPG" />
                </div>
                <div class="row">
                    <a href="#">Vand inchiriez teren</a>
                </div>
                <div class="row">
                    <div class="col-6"> 70 $</div>
                    <div class="col-6 fas fa-star"> </div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/navigatie.JPG" />
                </div>
                <div class="row">
                    <a href="#">Navigatie BMW</a>
                </div>
                <div class="row">
                    <div class="col-6">3 399 lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/trasport.JPG" />
                </div>
                <div class="row">
                    <a href="#">Servicii Trasport marfa</a>
                </div>
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6 fas fa-star">
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/pisica.JPG" />
                </div>
                <div class="row">
                    <a href="#">DJ Evenimente</a>
                </div>
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/chirie.JPG" />
                </div>
                <div class="row">
                    <a href="#">Inchiriez apartament</a>
                </div>
                <div class="row">
                    <div class="col-6">800lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-2">
                <div class="row">
                    <img src="picture/incarcator.JPG" />
                </div>
                <div class="row">
                    <a href="#">Incarcator Samsung fast</a>
                </div>
                <div class="row">
                    <div class="col-6"> 49 lei</div>
                    <div class="fas fa-star "></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/teren.JPG" />
                </div>
                <div class="row">
                    <a href="#">Vand inchiriez teren</a>
                </div>
                <div class="row">
                    <div class="col-6"> 70 $</div>
                    <div class="col-6 fas fa-star"> </div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/navigatie.JPG" />
                </div>
                <div class="row">
                    <a href="#">Navigatie BMW</a>
                </div>
                <div class="row">
                    <div class="col-6">3 399 lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/trasport.JPG" />
                </div>
                <div class="row">
                    <a href="#">Servicii Trasport marfa</a>
                </div>
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6 fas fa-star">
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/pisica.JPG" />
                </div>
                <div class="row">
                    <a href="#">DJ Evenimente</a>
                </div>
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
            <div class="col-2">
                <div class="row">
                    <img src="picture/chirie.JPG" />
                </div>
                <div class="row">
                    <a href="#">Inchiriez apartament</a>
                </div>
                <div class="row">
                    <div class="col-6">800lei</div>
                    <div class="col-6 fas fa-star"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <a href="#" class="ml-auto">vezi toate -></a>
        </div>

        <hr>

        <div class="row">

            <div class="col-8" style="color: #aaaaaa">Fii alaturi de noi si pe retele sociale:</div>
            <div class="col-4 ml-auto">
                <button type="button" class="btn btn-primary fa fa-check">Imi place 990K</button>
                <button type="button" >Urmariti</button>
                <button type="button" class="btn btn-primary fa fa-twitter">Tweet</button>
            </div>
        </div>
        <hr>
        <div class="row">
            <p>OLX.ro iti ofera posibilitatea de a publica anunturi gratuite pentru orasul tau si imprejurimile sale. Vei gasi usor pe OLX.ro
                anunturi gratuite interesante din Bucuresti, Ilfov si alte orase din tara si vei putea intra usor in legatura cu cei care le-au
                publicat. Pe OLX.ro te asteapta locuri de munca, apartamente si camere de inchiriat, masini second-hand si telefoane mobile la
                preturi mici. Daca vrei sa vinzi ceva vei putea adauga foarte usor anunturi gratuite.
                Daca vrei sa cumperi ceva aici vei putea gasi produsele care te intereseaza la preturi mai mici decat in orice magazin.</p>
        </div>
        <hr>
        <div class="row">
            <p>
                Categorii principale: Auto, moto si ambarcatiuni,	Imobiliare,	Locuri de munca,	Electronice si electrocasnice,	Moda si frumusete,
                Casa si gradina,
                Mama si copilul,	Sport, timp liber, arta, Animale de companie,	Agro si industrie,	Servicii, afaceri, echipamente firme
            </p>
        </div>
        <hr>
        <div class="row">
            <p>Cautari frecvente:
                <a href="#"><u>Schimb ,	Casa,	Bicicleta,	Tractor,	4x4,	Rulota,	Vand,	Audi a4,	Golf 4,	Atv,
                        Bmw,	Logan,	Teren,	Case,	Netflix,	Remorca,	Mobila,	Passat,	Iphone x,	Apartament </u> </a>
            </p>
            <style>     a {
                    COLOR: grey;
                    TEXT-DECORATION: none;
                    font-weight: normal
                } </style>
        </div>
        <hr>
        <div class="row">
            <?php include "parts/footer.php" ?>
        </div>





    </div>















</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>